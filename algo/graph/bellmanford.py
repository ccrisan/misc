
'''
Computes the shortest path from one source to all destinations in a graph.

Complexity: O[e + v]
'''


inf = float('inf')


class Vertex(object):
    def __init__(self, value):
        self.value = value


class Edge(object):
    def __init__(self, index1, index2, dist):
        self.index1 = index1
        self.index2 = index2
        self.dist = dist


def bellman_ford(vertices, edges, source_index):
    # initialize the distances
    dist = [inf] * len(vertices)
    prev = [None] * len(vertices)

    dist[source_index] = 0

    # relax the edges repeatedly
    for i in xrange(len(vertices) - 1): #@UnusedVariable
        relaxed = False
        for e in edges:
            if dist[e.index1] + e.dist < dist[e.index2]:
                dist[e.index2] = dist[e.index1] + e.dist
                prev[e.index2] = e.index1

                relaxed = True

            # this second if branch is necessary
            # for undirected graphs
            elif dist[e.index2] + e.dist < dist[e.index1]:
                dist[e.index1] = dist[e.index2] + e.dist
                prev[e.index1] = e.index2

                relaxed = True

        if not relaxed:
            break

    # check for negative distance cycles
    for e in edges:
        if dist[e.index1] + e.dist < dist[e.index2]:
            raise Exception('graph contains negative-weight cycles')

    return (dist, prev)


def show_distances(vertices, edges, dist, prev):
    for i in xrange(len(vertices)):
        print('distance to {v} is {d}'.format(v=vertices[i].value, d=dist[i]))

v1 = Vertex(1)
v2 = Vertex(2)
v3 = Vertex(3)
v4 = Vertex(4)
v5 = Vertex(5)
v6 = Vertex(6)
vertices = [v1, v2, v3, v4, v5, v6]

e1 = Edge(0, 1, 7)
e2 = Edge(0, 2, 9)
e3 = Edge(0, 5, 14)
e4 = Edge(1, 3, 15)
e5 = Edge(1, 2, 10)
e6 = Edge(2, 3, 11)
e7 = Edge(2, 5, 2)
e8 = Edge(3, 4, 6)
e9 = Edge(4, 5, 9)
edges = [e1, e2, e3, e4, e5, e6, e7, e8, e9]
        
        
(dist, prev) = bellman_ford(vertices, edges, 0)
show_distances(vertices, edges, dist, prev)
