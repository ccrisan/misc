
import os
import pydot


class Vertex(object):
    last_index = -1

    def __init__(self):
        Vertex.last_index += 1

        self.index = Vertex.last_index
        self.color = -1


class Edge(object):
    def __init__(self, vertex1, vertex2):
        self.vertex1 = vertex1
        self.vertex2 = vertex2


def colorize(vertices, edges):
    # compute the degrees - O[v]
    degs = [0] * len(vertices)
    max_deg = -1
    for e in edges:
        degs[e.vertex1.index] += 1
        degs[e.vertex2.index] += 1
        
        if degs[e.vertex1.index] > max_deg:
            max_deg = degs[e.vertex1.index]
        if degs[e.vertex2.index] > max_deg:
            max_deg = degs[e.vertex2.index]

    curr_deg = 0
    last_vertex_index = -1
    while curr_deg <= max_deg: # - O[d]
        # look for the first vertex with degree curr_deg - O[v]
        curr_vertex = None
        for i in xrange(last_vertex_index + 1, len(vertices)):
            if degs[vertices[i].index] == curr_deg:
                curr_vertex = vertices[i]
                last_vertex_index = i
                break
        
        if curr_vertex is None:
            # no such vertex found, pass to the next degree
            curr_deg += 1
            last_vertex_index = -1
            continue
        
        # at this point we found our current "minimum degree" vertex
        
        # look through siblings that have a lower or equal degree - O[v] * O[e]
        # find the minimum color that wasn't used by any of them
        max_color = -1 # infinity
        for v in vertices:
            if (degs[v.index] <= curr_deg) and (v != curr_vertex):
                # find edges that link v with curr_vertex - O[e]
                for e in edges:
                    if e.vertex1 == v and e.vertex2 == curr_vertex and e.vertex1.color > max_color:
                        max_color = e.vertex1.color
                    
                    elif e.vertex2 == v and e.vertex1 == curr_vertex and e.vertex2.color > max_color:
                        max_color = e.vertex2.color
        
        # assign the color to our vertex
        curr_vertex.color = max_color + 1


def show(vertices, edges):
    graph = pydot.Dot('MyGraph', graph_type='graph')
    gv_nodes = {}
    
    colors = ['black', 'red', 'orange', 'blue', 'green', 'magenta', 'brown']

    for v in vertices:
        gv_node = pydot.Node(chr(65 + v.index), color=colors[v.color + 1])
        gv_nodes[v.index] = gv_node
        graph.add_node(gv_node)

    for e in edges:
        gv_edge = pydot.Edge(gv_nodes[e.vertex1.index], gv_nodes[e.vertex2.index])
        graph.add_edge(gv_edge)

    graph.write_png('/tmp/graph.png')
    os.system('eog /tmp/graph.png')


a = Vertex()
b = Vertex()
c = Vertex()
d = Vertex()
e = Vertex()
f = Vertex()

ab = Edge(a, b)
ac = Edge(a, c)
be = Edge(b, e)
bc = Edge(b, c)
cd = Edge(c, d)
ed = Edge(e, d)
ef = Edge(e, f)

vertices = [a, b, c, d, e, f]
edges = [ab, ac, be, bc, cd, ed, ef]

colorize(vertices, edges)
show(vertices, edges)
