'''
Created on Aug 19, 2012

@author: ccrisan
'''

class Vertex(object):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)


class Edge(object):
    def __init__(self, index1, index2, weight):
        self.index1 = index1
        self.index2 = index2
        self.weight = weight


# this manager implements a priority queue
# based on a heap
class DistManager(object):
    def __init__(self, vertexes, start_index):
        self.dist = [float('inf')] * len(vertexes)
        self.dist[start_index] = 0

        self.heap = [i for i in xrange(len(vertexes)) if i != start_index]
        self.heap.insert(0, start_index)
        self.heap_index_map = [-1] * len(vertexes)
        for pos, index in enumerate(self.heap):
            self.heap_index_map[index] = pos

    def pop_min_index(self):
        if len(self.heap) == 0: # all vertexes are visited
            return -1
        
        min_index = self.heap[0]
        last_index = self.heap.pop()
        
        print('pop -> ' + str(min_index))

        # rebuild the min-heap

        if len(self.heap) == 0:
            return min_index
        
        self.heap[0] = last_index
        self.heap_index_map[self.heap[0]] = 0
            
        root_pos = 0

        while True:
            left_pos = root_pos * 2 + 1
            right_pos = root_pos * 2 + 2

            root_index = self.heap[root_pos]
            left_index = len(self.heap) > left_pos and self.heap[left_pos]
            right_index = len(self.heap) > right_pos and self.heap[right_pos]

            root_dist = self.dist[root_index]
            left_dist = self.dist[left_index] or float('inf')
            right_dist = self.dist[right_index] or float('inf')

            if left_dist < root_dist or right_dist < root_dist:
                if left_dist < right_dist:
                    # the smallest value is the one on the left
                    self.swap_heap(root_pos, left_pos)
                    root_pos = left_pos

                else:
                    # the smallest values is on the right
                    self.swap_heap(root_pos, right_pos)
                    root_pos = right_pos

            else:
                # else the heap property is satisfied
                break

        return min_index

    def get_dist(self, index):
        return self.dist[index]

    def set_dist(self, index, dist):
        print('set(' + str(index) + ', ' + str(dist) + ')')
        
        self.dist[index] = dist
        
        # rebuild the heap
        
        pos = self.heap_index_map[index]
        while pos > 0:
            root_pos = int((pos - 1) / 2)

            root_index = self.heap[root_pos]
            root_dist = self.dist[root_index]

            if dist < root_dist:
                self.swap_heap(root_pos, pos)
                pos = root_pos

            else:
                # else the heap property is satisfied
                break

    def swap_heap(self, pos1, pos2):
        temp = self.heap[pos1]
        self.heap[pos1] = self.heap[pos2]
        self.heap[pos2] = temp
        
        self.heap_index_map[self.heap[pos1]] = pos1
        self.heap_index_map[self.heap[pos2]] = pos2


v1 = Vertex(1)
v2 = Vertex(2)
v3 = Vertex(3)
v4 = Vertex(4)
v5 = Vertex(5)
v6 = Vertex(6)
vertexes = [v1, v2, v3, v4, v5, v6]

e1 = Edge(0, 1, 7)
e2 = Edge(0, 2, 9)
e3 = Edge(0, 5, 14)
e4 = Edge(1, 3, 15)
e5 = Edge(1, 2, 10)
e6 = Edge(2, 3, 11)
e7 = Edge(2, 5, 2)
e8 = Edge(3, 4, 6)
e9 = Edge(4, 5, 9)
edges = [e1, e2, e3, e4, e5, e6, e7, e8, e9]

prev = [-1] * len(vertexes)
dist_manager = DistManager(vertexes, start_index=0)

while True: # O(V)
    min_index = dist_manager.pop_min_index() # O(1)
    if min_index == -1: # all vertexes were visited
        break

    min_dist = dist_manager.get_dist(min_index)
    if min_dist == float('inf'): # remaining unvisited vertexes are unreachable
        break

    for e in edges: # O(E)
        if e.index1 == min_index:
            other_index = e.index2

        elif e.index2 == min_index:
            other_index = e.index1

        else:
            continue # not a neighbor

        new_dist = min_dist + e.weight
        if new_dist < dist_manager.get_dist(other_index):
            dist_manager.set_dist(other_index, new_dist) # O(log(V))
            prev[other_index] = min_index

for i in xrange(len(vertexes)):
    vertex = vertexes[i]
    dist = dist_manager.get_dist(i)
    print('distance to %s is %f' % (vertex, dist))
