'''
Created on Aug 25, 2012

@author: ccrisan
'''

class Vertex(object):
    def __init__(self, value):
        self.value = value
        self.parent = None
        self.rank = 0
        

class Edge(object):
    def __init__(self, index1, index2, dist):
        self.index1 = index1
        self.index2 = index2
        self.dist = dist
        self.used = False


# the data structure used here is called
# a "disjoint-set"

def find(vertex):
    if vertex.parent is None:
        return vertex
    
    else:
        vertex.parent = find(vertex.parent)
        
        return vertex.parent


def union(vertex1, vertex2):
    root1 = find(vertex1) # O(log(V)) or O(1) when applying path compression
    root2 = find(vertex2) # O(log(V))
    
    if root1 == root2:
        return False
    
    if root1.rank < root2.rank:
        root1.parent = root2
        
    elif root1.rank > root2.rank:
        root2.parent = root1
        
    else: # root1,rank == root2.rank
        root2.parent  = root1
        root1.rank += 1
    
    return True


A = Vertex('A') # 0
B = Vertex('B') # 1
C = Vertex('C') # 2
D = Vertex('D') # 3
E = Vertex('E') # 4
F = Vertex('F') # 5
G = Vertex('G') # 6
vertices = [A, B, C, D, E, F, G]

ab = Edge(0, 1, 7)
ad = Edge(0, 3, 5)
bc = Edge(1, 2, 8)
bd = Edge(1, 3, 9)
be = Edge(1, 4, 7)
ce = Edge(2, 4, 5)
de = Edge(3, 4, 15)
df = Edge(3, 5, 6)
ef = Edge(4, 5, 8)
eg = Edge(4, 6, 9)
fg = Edge(5, 6, 11)
edges = [ab, ad, bc, bd, be, ce, de, df, ef, eg, fg]

# sort the edges ascending by their distance
edges = sorted(edges, key=lambda e: e.dist)

for edge in edges: # O(E)
    v1 = vertices[edge.index1]
    v2 = vertices[edge.index2]

    edge.used = union(v1, v2) # O(log(V)) or O(1) when using path compression

for edge in edges:
    if not edge.used:
        continue
    
    v1 = vertices[edge.index1]
    v2 = vertices[edge.index2]
    
    print(v1.value + ' ' + v2.value)
