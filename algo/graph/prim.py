'''
Created on Aug 25, 2012

@author: ccrisan
'''

class Vertex(object):
    def __init__(self, value):
        self.value = value
        self.used = False


class Edge(object):
    def __init__(self, index1, index2, dist):
        self.index1 = index1
        self.index2 = index2
        self.dist = dist
        self.used = False


A = Vertex('A') # 0
B = Vertex('B') # 1
C = Vertex('C') # 2
D = Vertex('D') # 3
E = Vertex('E') # 4
F = Vertex('F') # 5
G = Vertex('G') # 6
vertices = [A, B, C, D, E, F, G]

ab = Edge(0, 1, 7)
ad = Edge(0, 3, 5)
bc = Edge(1, 2, 8)
bd = Edge(1, 3, 9)
be = Edge(1, 4, 7)
ce = Edge(2, 4, 5)
de = Edge(3, 4, 15)
df = Edge(3, 5, 6)
ef = Edge(4, 5, 8)
eg = Edge(4, 6, 9)
fg = Edge(5, 6, 11)
edges = [ab, ad, bc, bd, be, ce, de, df, ef, eg, fg]

# sort the edges ascending by their distance
edges = sorted(edges, key=lambda e: e.dist)
used_vertices = 1

vertices[0].used = True

while used_vertices < len(vertices):
    edge_used = False

    for edge in edges: # O(E)
        if edge.used:
            continue
        
        v1 = vertices[edge.index1]
        v2 = vertices[edge.index2]

        if v1.used and not v2.used:
            edge.used = True
            edge_used = True
            v2.used = True
            used_vertices += 1
            break

        elif v2.used and not v1.used:
            edge.used = True
            edge_used = True
            v1.used = True
            used_vertices += 1
            break

        else:
            # ignore the edge for now
            continue

    if not edge_used:
        print('the graph is not connected')

        break

for edge in edges:
    if not edge.used:
        continue

    v1 = vertices[edge.index1]
    v2 = vertices[edge.index2]

    print(v1.value + ' ' + v2.value)
