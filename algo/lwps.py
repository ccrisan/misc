#!/usr/bin/env python

import sys
import StringIO

def longest_parenthesized_len(input, ignore_invalid_chars=True):
    """Compute the length of the largest well-parenthesized sequence
    from a given input string.

    >>> longest_parenthesized_len('')
    0
    >>> longest_parenthesized_len('()')
    2
    >>> longest_parenthesized_len('))(())')
    4
    >>> longest_parenthesized_len('(()())')
    6
    >>> longest_parenthesized_len('))))')
    0
    >>> longest_parenthesized_len('((((')
    0
    >>> longest_parenthesized_len(')(')
    0
    >>> longest_parenthesized_len('((some invalid chars))()')
    6
    >>> longest_parenthesized_len('(())#(())', ignore_invalid_chars=False)
    Traceback (most recent call last):
        ...
    Exception: unexpected character "#" at position 4
    """

    # the stack contains the positions at which brackets are opened;
    # at any time, the length of the stack represents the current bracket level;
    stack = []

    # the current position within the input string
    pos = 0

    # going below bracket level 0 (i.e. negative stack length) is not possible,
    # therefore last_start_pos will tell the last position
    # at which we started with an empty stack looking for a valid sequence
    last_start_pos = 0

    # remember the maximal length seen so far
    max_len = 0

    # in order to uniformly use the read() method on both streams and string,
    # we transform string inputs into StringIO 
    if isinstance(input, basestring):
        input = StringIO.StringIO(input)

    while True:
        # read one char from the input stream, until no more chars are available
        c = input.read(1)
        if not c:
            break

        if c == '(':
            # remember the position at which we increased the bracket level,
            # by pushing it to the stack
            stack.append(pos)

        elif c == ')':
            if len(stack) > 0:
                # a closing bracket with a non-empty stack increases the current sequence length

                stack.pop()
                if len(stack) == 0:
                    max_len = max(max_len, pos - last_start_pos + 1)

                else:
                    max_len = max(max_len, pos - stack[-1])

            else:
                # a closing bracket with an empty stack resets the start position 
                last_start_pos = pos + 1

        else: # we only accept brackets
            if not ignore_invalid_chars:
                raise Exception('unexpected character "%(c)s" at position %(pos)d' % {'c': c, 'pos': pos})
            
            else:
                pos -= 1

        pos += 1

    return max_len


if __name__ == '__main__':
    try:
        arg = sys.argv[1]

    except IndexError:
        print('Usage: %(prog)s [-t|filename]' % {'prog': sys.argv[0]})

        sys.exit()

    if arg == '-t': # run a couple of tests using doctest
        import doctest
        doctest.testmod(verbose=True)

    else: # use file given in arg as input
        f = open(arg)
        max_len = longest_parenthesized_len(f)

        print(max_len)
