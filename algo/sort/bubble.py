
''' 
    Idea:
        - rabbits are big elements at the beginning of the list - they go fast to the end
        - turtles are small elements at the end of the list - they come slowly to the beginning

    Average Time Complexity: O[n2]
    Worst Case Time Complexity: O[n2] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[n] 
    Efficient For: very few, almost sorted elements
    
    Comparative: yes
    In Place: yes
    Stable: yes
    Online: no
    Constraints: -
'''

def swap(a, i1, i2):
    tmp = a[i1]
    a[i1] = a[i2]
    a[i2] = tmp


def bubble_sort(a, count):
    while True:
        swapped = False
        new_count = count
        for i in xrange(1, count):
            if a[i - 1] > a[i]:
                swap(a, i - 1, i)
                swapped = True
                new_count = i

        if not swapped:
            break
    
        count = new_count
        
    return a


a = [38, 27, 43, 3, 9, 82, 10]
a = bubble_sort(a, len(a))
print(a)
