
''' 
    Idea:
        - a bidirectional version of bubble sort
        - solves the turtles issue

    Average Time Complexity: O[n2]
    Worst Case Time Complexity: O[n2] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[n]
    Efficient For: very few, almost sorted elements
    
    Comparative: yes
    In Place: yes
    Stable: yes
    Online: no
    Constraints: -
'''

def swap(a, i1, i2):
    tmp = a[i1]
    a[i1] = a[i2]
    a[i2] = tmp


def cocktail_sort(a, count):
    begin = 0
    end = count - 1

    while True:
        swapped = False
        for i in xrange(begin + 1, end + 1):
            if a[i - 1] > a[i]:
                swap(a, i - 1, i)
                swapped = True
                end = i - 1

        if not swapped:
            break

        swapped = False
        for i in xrange(end, begin, -1):
            if a[i - 1] > a[i]:
                swap(a, i - 1, i)
                swapped = True
                begin = i

        if not swapped:
            break

    return a


a = [38, 27, 43, 3, 9, 82, 10]
a = cocktail_sort(a, len(a))
print(a)
