
''' 
    Idea:
        - solves the turtles issue by using a decreasing gap between two swapped elements
        - the gap is divided by the shrink factor with each iteration
        - with a shrink factor around 1.3, there are only three possible ways
        for the list of gaps to end: (9, 6, 4, 3, 2, 1), (10, 7, 5, 3, 2, 1), or (11, 8, 6, 4, 3, 2, 1) 

    Average Time Complexity: O[n*log(n)]
    Worst Case Time Complexity: O[n2] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[n]
    Efficient For: ?
    
    Comparative: yes
    In Place: yes
    Stable: no
    Online: no
    Constraints: -
'''

def swap(a, i1, i2):
    tmp = a[i1]
    a[i1] = a[i2]
    a[i2] = tmp


def comb_sort(a, count):
    gap = count
    
    while True:
        gap = max(1, int(gap / 1.247330950103979))
        swapped = False
        i = 0
        while i + gap < count:
            if a[i] > a[i + gap]:
                swap(a, i, i + gap)
                swapped = True
            
            i += 1

        if gap == 1 and not swapped:
            break

    return a


a = [38, 27, 43, 3, 9, 82, 10]
a = comb_sort(a, len(a))
print(a)
