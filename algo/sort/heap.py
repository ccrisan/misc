
''' 
    Idea:
        - works by building and maintaining a binary heap (a complete binary tree)
        - a heapify() function initially builds the heap within the original array
        - a sift_down() function is used to place a root in a proper position (when building and maintaining the heap)
        - the biggest element is always the root of the heap (in the first position)
        - after building the heap, for each position in the array:
            - swap the first and the last elements (max goes to the last position)
            - rebuild the heap for the remaining elements

    Average Time Complexity: O[n*log(n)]
    Worst Case Time Complexity: O[n*log(n)] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[n*log(n)] 
    Efficient For: general purpose, better worst case complexity than quick sort 
    
    Comparative: yes
    In Place: yes
    Stable: no
    Online: no
    Constraints: -
'''

def swap(a, i1, i2):
    tmp = a[i1]
    a[i1] = a[i2]
    a[i2] = tmp


def heapify(a, count):
    # start is assigned the index in a of the last parent node
    start = count / 2 - 1

    while start >= 0:
        # sift down the node at index start to the proper place such that all nodes below
        # the start index are in heap order
        sift_down(a, start, count - 1)
        start = start - 1
     
    # after sifting down the root all nodes/elements are in heap order

 
def sift_down(a, start, end):
    root = start
    
    while root * 2 + 1 <= end:
        left = root * 2 + 1
        right = left + 1
        swap_with = root
        
        if a[root] < a [left]:
            swap_with = left
        
        if (right <= end and a[swap_with] <= a[right]):
            swap_with = right
        
        if swap_with != root:
            swap(a, root, swap_with)
            root = swap_with
        
        else:
            return


def heap_sort(a, count):
    # first place a in max-heap order
    heapify(a, count)
    
    end = count - 1
    while end > 0:
        # swap the root(maximum value) of the heap with the last element of the heap
        swap(a, end, 0)
        # decrease the size of the heap by one so that the previous max value will stay in its proper placement
        end = end - 1
        # put the heap back in max-heap order
        sift_down(a, 0, end)

 
a = [8, 1, 17, 20, 6, 9, 4, 14]
heap_sort(a, len(a))
print(a)
