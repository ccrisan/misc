
'''
    Idea:
        - the sorted part is always kept on the beginning of the array
        - for each position in the array (from 1 to n - 1)
            - shift elements to the right until we encounter a smaller element
            - insert the current element right after this smaller element
    
    Average Time Complexity: O[n2]
    Worst Case Time Complexity: O[n2]
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: tends towards O[n]
    Efficient For: small data sets

    Comparative: yes
    In Place: yes
    Stable: yes
    Online: yes
    Constraints: -
'''

def insertion_sort(a, count):
    for sort_pos in xrange(1, count):
        sort_value = a[sort_pos]
        curr_pos = sort_pos - 1

        while (curr_pos >= 0) and (sort_value < a[curr_pos]):
            a[curr_pos + 1] = a[curr_pos]
            curr_pos -= 1

        a[curr_pos + 1] = sort_value

    return a


a = [3, 7, 4, 9, 5, 2, 6, 1, 8]
a = insertion_sort(a, len(a))
print(a)
