
''' 
    Idea:
        - split the array into two parts
        - apply the function to each of the parts, recursively, until we have a list of 0 or 1 elements 
        - merge the two parts using a function which always chooses the smallest element from the beginning of the two lists

    Average Time Complexity: O[n*log(n)]
    Worst Case Time Complexity: O[n*log(n)] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[n*log(n)] 
    Efficient For: lists with efficient sequential access
    
    Comparative: yes
    In Place: yes
    Stable: yes
    Online: yes
    Constraints: -
'''

def merge(a, aux, start1, end1, start2, end2):
    aux_pos = start1
    origin = start1
    
    while end1 - start1 >= 0 or end2 - start2 >= 0:
        if end1 - start1 >= 0:
            if end2 - start2 >= 0:
                if a[start1] < a[start2]:
                    aux[aux_pos] = a[start1]
                    start1 += 1
                
                else:
                    aux[aux_pos] = a[start2]
                    start2 += 1
            
            else:
                aux[aux_pos] = a[start1]
                start1 += 1
            
        else:
            if end2 - start2 >= 0:
                aux[aux_pos] = a[start2]
                start2 += 1
                
        aux_pos += 1
    
    i = origin
    while i < aux_pos:
        a[i] = aux[i]
        i += 1
    

def merge_sort(a, start, end, aux):
    if end > start:
        middle = (start + end) // 2
        merge_sort(a, start, middle, aux)
        merge_sort(a, middle + 1, end, aux)
    
        merge(a, aux, start, middle, middle + 1, end)
    
    return aux


a = [38, 27, 43, 3, 9, 82, 10]
a = merge_sort(a, 0, len(a) - 1, [None] * len(a))
print(a)
