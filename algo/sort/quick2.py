
from random import randint


''' 
    Idea:
        - divide the array into two parts, separated by a pivot element
        - the choice of the pivot could be the half of the array
        - a partitioning function will help arranging the smaller elements
            to the left of the pivot and the bigger ones to the right, by swapping
        - recursively apply this partitioning to the input array

    Average Time Complexity: O[n*log(n)]
    Worst Case Time Complexity: O[n2] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[n*log(n)] 
    Efficient For: general purpose, better worst case complexity than quick sort 
    
    Comparative: yes
    In Place: yes
    Stable: no
    Online: no
    Constraints: -
'''

def swap(a, i1, i2):
    tmp = a[i1]
    a[i1] = a[i2]
    a[i2] = tmp


def partition(a, start, end):
    pivot = (start + end) // 2
    pivot_value = a[pivot]

    i = start
    j = end

    while i <= j:
        while pivot_value > a[i]:
            i += 1

        while pivot_value < a[j]:
            j -= 1

        if i <= j:
            swap(a, i, j)
            i += 1
            j -= 1

    return i


def quick_sort(a, start, end):
    if end > start:
        pivot = partition(a, start, end)

        quick_sort(a, start, pivot - 1)
        quick_sort(a, pivot, end)
    
    return a


def quick_sort2(a, start, end):
    if start >= end:
        return
    
    swap(a, start, randint(start, end))
    
    m = start
    i = start + 1
    while i <= end:
        if a[i] < a[start]:
            m += 1
            swap(a, m, i)
        
        i += 1

    swap(a, start, m)
    
    quick_sort2(a, start, m - 1)
    quick_sort2(a, m + 1, end)

#a = [3, 7, 8, 5, 2, 1, 9, 5, 4]
a = [31, 41, 59, 26, 53]
quick_sort2(a, 0, len(a) - 1)
print(a)

