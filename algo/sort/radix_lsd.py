
''' 
    Idea:
        - all keys are practically left padded
        - shorter keys come before longer keys, keys of same length are sorted lexicographically
        - suitable for sorting numbers
        
        - create a bucket for each possible digit (including a blank digit)
        - starting with the most significant digit, put each element in the corresponding bucket
        - apply this algorithm to each bucket, recursively
        - merge the buckets to obtain the result
    
    Average Time Complexity: O[kn]
    Worst Case Time Complexity: O[kn] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[kn]
    Efficient For: keys in a positional notation, digit/bit grouping
    
    Comparative: no
    In Place: no
    Stable: no
    Online: no
    Constraints: the keys must be in a positional notation
'''

from math import log

def lsd_radix_sort(a, radix):
    max_number = max(a)
    digit_count = int(log(max_number, radix)) + 1

    for digit_no in xrange(digit_count):
        # split into buckets
        buckets = [[] for i in xrange(radix)] #@UnusedVariable
        for v in a:
            digit = int((v // radix ** digit_no) % radix)
            buckets[digit].append(v)

        # merge them back
        new_a = []
        for bucket in buckets:
            new_a.extend(bucket)

        a = new_a

    return a


a = [170, 45, 75, 90, 802, 24, 2, 66]
a = lsd_radix_sort(a, 10)
print(a)
