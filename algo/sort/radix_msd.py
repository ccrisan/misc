
''' 
    Idea:
        - all keys are practically left padded
        - all keys are sorted lexicographically
        - suitable for string
        
        - create a bucket for each possible digit (including a blank digit)
        - starting with the most significant digit, put each element in the corresponding bucket
        - apply this algorithm to each bucket, recursively
        - merge the buckets to obtain the result
    
    Average Time Complexity: O[kn]
    Worst Case Time Complexity: O[kn] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[kn]
    Efficient For: keys in a positional notation, digit/bit grouping
    
    Comparative: no
    In Place: no
    Stable: no
    Online: no
    Constraints: the keys must be in a positional notation
'''

def msd_radix_sort(a):
    max_level = len(max(a, key=lambda v: len(v))) - 1

    def msb_radix_sort_rec(a, level):
        if len(a) < 2:
            return a

        # split into buckets
        buckets = [[] for i in xrange(27)] #@UnusedVariable
        for v in a:
            pos = level - max_level + len(v) - 1
            if pos >= 0:
                buckets[ord(v[pos]) - ord('a') + 1].append(v)

            else:
                buckets[0].append(v)

        new_a = []
        for bucket in buckets:
            if level < max_level:
                bucket = msb_radix_sort_rec(bucket, level + 1)

            # merge it back
            new_a.extend(bucket)

        return new_a
    
    return msb_radix_sort_rec(a, 0)

a = ['mama', 'are', 'mere', 'si', 'e', 'tare', 'fericita']
a = msd_radix_sort(a)
print(a)
