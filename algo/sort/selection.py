
'''
    Idea:
        - for each position in the list
            - find the minimal element in the right side of the array
            - swap it with the current element

    Average Time Complexity: O[n2]
    Worst Case Time Complexity: O[n2] 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[n2]
    Efficient For:
        - small data sets
        - linked lists, if implemented using insert/remove instead of swap
    
    Comparative: yes
    In Place: yes
    Stable: no, but can be if implemented using insert/remove instead of swap
    Online: no
    Constraints: -
'''

def selection_sort(a):
    for i in xrange(0, len(a)):
        min_pos = i
        min_value = a[min_pos]
        
        # find the minimal value
        for j in xrange(i, len(a)):
            if a[j] < min_value:
                min_pos = j
                min_value = a[j]
            
        # swap the minimal value with the first unsorted element
        a[min_pos] = a[i]
        a[i] = min_value
    
    return a


a = [3, 7, 4, 9, 5, 2, 6, 1, 8]
a = selection_sort(a)
print(a)
