
''' 
    Idea:
        - is basically an insertion sort that operates on subarrays
        made from elements at every h-th position (h being the gap)
        - the subarrays are initially short; later they are longer but almost ordered

    Average Time Complexity: depends on the gap sequence
    Worst Case Time Complexity: depends on the gap sequence 
    Worst Case Space Complexity: O[1]
    
    Almost Sorted Behavior: O[n]
    Efficient For: ?
    
    Comparative: yes
    In Place: yes
    Stable: no
    Online: no
    Constraints: -
'''

def swap(a, i1, i2):
    tmp = a[i1]
    a[i1] = a[i2]
    a[i2] = tmp


#gaps = [701, 301, 132, 57, 23, 10, 4, 1]
gaps = [1]

def shell_sort(a, count):
    for gap in gaps:
        for i in xrange(gap, count):
            tmp = a[i]
            j = i
            while (j >= gap) and (a[j - gap] > tmp):
                a[j] = a[j - gap]
                j -= gap
            a[j] = tmp

    return a


a = [38, 27, 43, 3, 9, 82, 10]
a = shell_sort(a, len(a))
print(a)
