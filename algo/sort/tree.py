
''' 
    Idea:

    Average Time Complexity: O[n*log(n)]
    Worst Case Time Complexity: O[n2]
    Worst Case Space Complexity: ?
    
    Almost Sorted Behavior: O[n2]
    Efficient For: ?
    
    Comparative: yes
    In Place: no
    Stable: no
    Online: yes
    Constraints: -
'''

import os
import pydot


class Node(object):
    def __init__(self, left, right, key):
        self.left = left
        self.right = right
        self.key = key


def search(node, key):
    if node is None:
        return None

    elif key == node.key:
        return node

    elif key < node.key:
        return search(node.left, key)

    else:
        return search(node.right, key)


def insert(node, key):
    if node is None:
        return Node(None, None, key)

    if key < node.key:
        node.left = insert(node.left, key)

    else:
        node.right = insert(node.right, key)
    
    return node


def get_depth(node):
    if node is None:
        return 0

    else:
        return 1 + max(get_depth(node.left), get_depth(node.right))


def show(node):
    graph = pydot.Dot('MyTree', graph_type='digraph')
    
    def show_rec(node, color='black'):
        gv_node = pydot.Node(str(id(node)), label=str(node.key), color=color)
        graph.add_node(gv_node)
        
        if node.left:
            gv_left_node = show_rec(node.left, color='red')
        
        else:
            gv_left_node = pydot.Node(str(id(node)) + '-left', label='', color='lightgray')
            graph.add_node(gv_left_node)
            
        if node.right:
            gv_right_node = show_rec(node.right, color='blue')
        
        else:
            gv_right_node = pydot.Node(str(id(node)) + '-right', label='', color='lightgray')
            graph.add_node(gv_right_node)
            
        gv_left_edge = pydot.Edge(gv_node, gv_left_node)
        graph.add_edge(gv_left_edge)
        
        gv_right_edge = pydot.Edge(gv_node, gv_right_node)
        graph.add_edge(gv_right_edge)
        
        return gv_node

    if node:
        show_rec(node)
    
    graph.write_png('/tmp/tree.png')
    os.system('eog /tmp/tree.png')


#a = [38, 27, 43, 3, 9, 82, 10]
#a = comb_sort(a, len(a))
#print(a)

tree = None
tree = insert(tree, 7)
tree = insert(tree, 3)
tree = insert(tree, 11)
tree = insert(tree, 5)

print(get_depth(tree))
show(tree)
