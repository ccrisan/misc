#!/bin/bash

# TODO use crontab for regular checkings
# TODO add functionality for respawning scripts with start|stop|restart args
# TODO implement networking support
# TODO implement installing software support

set -e # exit on first error

command="$1"
subcommand="$2"


# config file functions

function get_config() {
    file="$1"
    name="$2"
    
    if [ -z "$file" ] || [ -z "$name" ]; then
        return 1
    fi
    
    if ! [ -r "$file" ]; then
        return
    fi
    
    # escape name for sed
    name=$(echo ${name} | sed -e 's/[\/&]/\\&/g')
    
    q='"' # quote workaround to avoid impossible escapings
    cat "$file" | sed -ne "s/^\s*\(${name}\(\s\|=\)\+\)$q\?\([^$q]*\)$q\?/\3/p"
}

function set_config() {
    file="$1"
    name="$2"
    value="$3"
    opts="$4"
    
    if [ -z "$file" ] || [ -z "$name" ] || [ -z "$value" ] || [ -z "$opts" ]; then
        return 1
    fi
    
    if ! [ -r "$file" ]; then
        echo "" > "$file" # create an empty file
    fi
    
    # escape name and value for sed
    name=$(echo ${name} | sed -e 's/[\/&]/\\&/g')
    value=$(echo ${value} | sed -e 's/[\/&]/\\&/g')
    
    if [[ "$opts" == *'"'* ]]; then
        value="${q}${value}${q}"
    fi
        
    q='"' # quote workaround to avoid impossible escapings
    if grep -e "#\?\s*${name}\(\s\+\|=\)" "$file" >/dev/null 2>&1; then # config name exists
        sed -ie "s/#\?\s*\(${name}\(\s\|=\)\+\)$q\?\([^$q]*\)$q\?/\1${value}/" "${file}"
    else # config name does not exist
        line="$name "
        if [[ "$opts" == *"="* ]]; then # equal sign in options
            line="${line}= "
        fi
        
        line="${line}${value}"
        
        echo "$line" >> "$file"
    fi
}

function disable_config() {
    file="$1"
    name="$2"
    
    if [ -z "$file" ] || [ -z "$name" ]; then
        return 1
    fi
    
    if ! [ -r "$file" ]; then
        return 0 # file inexistent - config presumably disabled by default
    fi
    
    sed -ie "s/^\s*\(${name}\(\s\|=\)\+\)/#\1/" "${file}"
}


# rc.local functions

function rclocal_command_enabled() {
    command="$1"
    if [ -z "$command" ]; then
        return 1
    fi

    if list_rclocal_commands | grep "$command" >/dev/null 2>&1; then
        return 0
    else
        return 1
    fi
}

function enable_rclocal_command() {
    user="$1"
    opts="$2"
    command="$3"
    args="${@:4}"
    if [ -z "$command" ]; then
        return 1
    fi
    
    if rclocal_command_enabled "$command"; then
        return # already enabled
    fi
    
    if [ "$user" == "root" ]; then
        line="test -x $command && $command $args"
    else
        line="test -x $command && sudo -iu $user $command $args"
    fi
    
    if [[ "$opts" == *"l"* ]]; then
        name=$(basename $command)
        name=${name%%.*}
        line="$line >/var/log/$name.log 2>&1"
    fi
    
    if [[ "$opts" == *"b"* ]]; then
        line="$line &"
    fi
    
    line=$(echo $line | sed -e 's/[\/&]/\\&/g') # escape for sed
    sed -ie "s/exit 0/$line\nexit 0/" /etc/rc.local
}

function disable_rclocal_command() {
    command="$1"
    if [ -z "$command" ]; then
        return 1
    fi

    if ! rclocal_command_enabled "$command"; then
        return # not enabled
    fi
    
    command=$(echo $command | sed -e 's/[\/&]/\\&/g') # escape for sed
    sed -ie "N;s/\ntest\s\+-x\s\+$command.*//" /etc/rc.local
}

function list_rclocal_commands() {
    sed -ne "s/test\s\+-x\s\+\([^ ]\+\).*/\1/p" /etc/rc.local
}


# misc functions

function check_raspbian() {
    cat /etc/issue | grep Raspbian
}

function print_usage() {
    cmd="$1"

    if [ -z "$cmd" ]; then
        echo "This script is intended to help setting up a server on a Raspberry PI with Raspbian."
        echo "usage: $0 <commamd> [options...]"
        echo "available commands:"
    fi
    
    if [ -z "$cmd" ] || [ "$cmd" == "coreinfo" ]; then
        echo "    coreinfo - displays Raspberry PI core information (frequency, temperature, etc)"
        echo ""
    fi
    
    if [ -z "$cmd" ] || [ "$cmd" == "watchdog" ]; then
        echo "    watchdog enable - enables the BCM2708 watchdog"
        echo "    watchdog disable - disables the BCM2708 watchdog"
        echo "    watchdog status - tells the status of the BCM2708 watchdog"
        echo ""
    fi

    if [ -z "$cmd" ] || [ "$cmd" == "softwatchdog" ]; then
        echo "    softwatchdog add <command> <interval> <count>"
        echo "        installs a script that reboots if a <command> fails for <count> times, when checked every <interval> seconds"
        echo "        (e.g. $0 softwatchdog add \"sudo -iu pi /usr/local/bin/hdd-test.sh -q\" 60 4)"
        echo "    softwatchdog rem <command>"
        echo "        removes the script that reboots if <command> fails"
        echo "        (e.g. $0 softwatchdog rem \"sudo -iu pi /usr/local/bin/hdd-test.sh -q\")"
        echo "    softwatchdog list"
        echo "        lists the commands that reboot upon failure"
        echo ""
    fi

    if [ -z "$cmd" ] || [ "$cmd" == "respawn" ]; then
        echo "    respawn add <command> <interval>"
        echo "        installs a script that respawns <command> if not running, checking it every <interval> seconds"
        echo "        (e.g. $0 respawn add \"sudo -iu pi /opt/myserver/myserver.sh -c /opt/myserver/myserver.conf >/var/log/myserver.log\" 10)"
        echo "    respawn rem <command>"
        echo "        removes the script that respawns <command>"
        echo "        (e.g. $0 respawn rem \"sudo -iu pi /opt/myserver/myserver.sh -c /opt/myserver/myserver.conf >/var/log/myserver.log\")"
        echo "    respawn list"
        echo "        lists the commands that are respawned"
        echo ""
    fi

    if [ -z "$cmd" ] || [ "$cmd" == "network" ]; then
        echo "    network wired enable <ip> <mask> [gw] [ns]"
        echo "    network wired enable dhcp"
        echo "        enables wired network and configures it with given parameters"
        echo "        (e.g. $0 network wired enable 192.168.1.3 24 192.168.1.1 8.8.8.8)"
        echo "    network wired disable"
        echo "        disables wired network"
        echo "    network wireless enable <ssid> <psk> <ip> <mask> [gw] [ns]"
        echo "    network wireless enable <ssid> <psk> dhcp"
        echo "        enables wireless network and configures it with given parameters"
        echo "        (e.g. $0 network wireless enable mynet mypassword dhcp)"
        echo "    network wireless disable"
        echo "        disables wireless network"
        echo "    network wired|wireless status"
        echo "        tells the status of the given network interface"
        echo ""
    fi

    if [ -z "$cmd" ] || [ "$cmd" == "overclock" ]; then
        echo "    overclock disable"
        echo "        disables overclocking"
        echo "    overclock normal"
        echo "        enables normal overclocking (so-called \"turbo mode\")"
        echo "    overclock hardcore"
        echo "        enables full overclocking, including overvoltage - use with caution, voids warranty!"
        echo "    overclock status"
        echo "        tells the current overclocking status"
        echo ""
    fi

    if [ -z "$cmd" ] || [ "$cmd" == "strip" ]; then
        echo "    strip gfx - removes all graphics-related packages from the system"
        echo "    strip full - reduces the packages to a bare minimum"
        echo ""
    fi

    if [ -z "$cmd" ] || [ "$cmd" == "install" ]; then
        echo "    install utils - installs various utilities (htop, tcpdump etc)"
        echo "    install proftpd - installs and configures proftpd"
        echo "    install nginx - installs and configures nginx"
        echo "    install samba - installs and configures samba"
        echo "    install deluge - installs and configures deluge"
        echo ""
    fi

    if [ -z "$cmd" ] || [ "$cmd" == "backup" ]; then
        echo "    backup <tarfile> [exclude path...]"
        echo "        creates a backup tarball of the filesystem, exluding specified paths"
        echo ""
    fi
}

function print_reboot_msg() {
    echo "Don't forget to reboot so the changes can take effect!"
}


# main functions

function coreinfo() {
    subcmd="$1"
    if [ "$subcmd" == "help" ]; then
        print_usage coreinfo
        return 1
    fi

    function print() {
        test -z "$3" && len="10" || len="$3"
        printf "%${len}s%s\n" "$1" "$2"
    }

    echo "Reading version..."
    date=$(vcgencmd version | head -n 1)
    ver=$(vcgencmd version | tail -n 1)
    print "date: " "${date}"
    print "version: " "${ver}"
    echo ""

    echo "Reading clocks..."
    for src in arm core h264 isp v3d uart pwm emmc pixel vec hdmi dpi; do
        val=$(vcgencmd measure_clock ${src} | cut -d '=' -f 2)
        val="$((${val} / 1000000)).$((${val} % 1000000))"
        print "${src}: " "${val} MHz"
    done
    echo ""

    echo "Reading voltages..."
    for id in core sdram_c sdram_i sdram_p; do
        val=$(vcgencmd measure_volts ${id} | cut -d '=' -f 2)
        print "${id}: " "${val}"
    done
    echo ""

    echo "Reading temperature..."
    val=$(vcgencmd measure_temp | cut -d '=' -f 2)
    print "soc: " "${val}"
    echo ""

    echo "Reading codecs status..."
    for id in 264 MPG2 WVC1 MPG4 MJPG WMV9; do
        val=$(vcgencmd codec_enabled ${id} | cut -d '=' -f 2)
        print "${id}: " "${val}"
    done
    echo ""

    echo "Reading memory configuration..."
    for id in arm gpu; do
        val=$(vcgencmd get_mem ${id} | cut -d '=' -f 2)
        print "${id}: " "${val}b"
    done
    echo ""

    echo "Reading config values..."
    vcgencmd get_config int | while read line; do parts=($(echo ${line} | tr '=' ' ')); print "${parts[0]}: " "${parts[1]}" 21; done
}

function watchdog() {
    subcmd="$1"
    
    case "$subcmd" in
        enable)
            echo "enabling watchdog..."
            apt-get -y install watchdog
            update-rc.d watchdog defaults
            set_config "/etc/default/watchdog" "run_watchdog" "1" '='
            set_config "/etc/default/watchdog" "watchdog_module" "bcm2708_wdog" '='
            set_config "/etc/watchdog.conf" "max-load-1" "30" '='
            set_config "/etc/watchdog.conf" "max-load-5" "20" '='
            set_config "/etc/watchdog.conf" "max-load-15" "10" '='
            set_config "/etc/watchdog.conf" "min-memory" "1" '='
            set_config "/etc/watchdog.conf" "watchdog-device" "/dev/watchdog" '='
            set_config "/etc/watchdog.conf" "interval" "5" '='
            set_config "/etc/watchdog.conf" "realtime" "yes" '='
            set_config "/etc/watchdog.conf" "priority" "1" '='

            print_reboot_msg
            ;;
        
        disable)
            echo "disabling watchdog..."
            update-rc.d -f watchdog remove
            apt-get -y remove watchdog
            
            print_reboot_msg
            ;;

        status)
            if dpkg -s watchdog >/dev/null 2>&1 && [ -r /etc/rc2.d/S*watchdog ]; then
                echo "enabled"
            else
                echo "disabled"
            fi
            ;;

        *)
            print_usage watchdog
            return 1
    esac
}

function softwatchdog() {
    subcmd="$1"
    
    case "$subcmd" in
        add)
            target_cmd="$2"
            interval=$3
            count=$4
            
            if [ -z "$count" ]; then
                print_usage softwatchdog
                return 1
            fi
        
            echo "adding $target_cmd for softwatchdog..."
            
            name=$(basename "$target_cmd")
            name=${name%%.*}
            file=/usr/local/bin/watch-$name.sh

            echo "#!/bin/bash" > $file
            echo "softwatchdog_cmd=\"$target_cmd\"" >> $file
            echo "softwatchdog_count=\"$count\"" >> $file
            echo "softwatchdog_interval=\"$interval\"" >> $file
            echo "while true; do" >> $file
            echo '    sleep $softwatchdog_interval' >> $file
            echo '    for ((i = 0; i < $softwatchdog_count; i++)); do' >> $file
            echo '        if $softwatchdog_cmd >/dev/null 2>&1; then' >> $file
            echo '            continue 2' >> $file
            echo '        fi' >> $file
            echo '    done' >> $file
            echo '    logger -s "softwatchdog: command $softwatchdog_cmd failed, rebooting..."' >> $file
            echo '    reboot' >> $file
            echo 'done' >> $file

            chmod +x "$file"

            enable_rclocal_command "root" "lb" "$file"
            
            print_reboot_msg
            ;;

        rem)
            target_cmd="$2"
            
            if [ -z "$target_cmd" ]; then
                print_usage softwatchdog
                return 1
            fi

            name=$(basename "$target_cmd")
            name=${name%%.*}
            file=/usr/local/bin/watch-$name.sh

            if [ -f "$file" ]; then
                echo "removing $target_cmd from softwatchdog..."
                rm "$file"
            fi
            
            disable_rclocal_command "$file"
            
            print_reboot_msg
            ;;

        list)
            echo "softwatchdog commands:"

            for file in /usr/local/bin/watch-*.sh; do
                if rclocal_command_enabled "$file"; then
                    cat "$file" | q='"' sed -ne "s/softwatchdog_cmd=\"\(.\+\)\"/\1/p"
                fi
            done
            ;;

        *)
            print_usage softwatchdog
            exit 1
    esac
}

function respawn() {
    subcmd="$1"
    
    case "$subcmd" in
        add)
            target_cmd="$2"
            interval=$3
            
            if [ -z "$interval" ]; then
                print_usage respawn
                return 1
            fi
            
            echo "adding $target_cmd for respawning..."
        
            name=$(basename "$target_cmd")
            name=${name%%.*}
            file=/usr/local/bin/respawn-$name.sh

            echo "#!/bin/bash" > $file
            echo "respawn_cmd=\"$target_cmd\"" >> $file
            echo "respawn_interval=\"$interval\"" >> $file
            echo "while true; do" >> $file
            echo '    sleep $respawn_interval' >> $file
            echo '    if ! ps aux | grep -e "$respawn_cmd" | grep -v grep >/dev/null 2>&1; then' >> $file
            echo '        logger -s "respawn: command $respawn_cmd not running, starting it..."' >> $file
            echo '        $respawn_cmd &' >> $file
            echo '    fi' >> $file
            echo 'done' >> $file

            chmod +x "$file"

            enable_rclocal_command "root" "lb" "$file"
            
            print_reboot_msg
            ;;

        rem)
            target_cmd="$2"
            
            if [ -z "$target_cmd" ]; then
                print_usage respawn
                return 1
            fi
        
            name=$(basename "$target_cmd")
            name=${name%%.*}
            file=/usr/local/bin/respawn-$name.sh

            if [ -f "$file" ]; then
                echo "removing $target_cmd from respawning..."
                rm "$file"
            fi
            
            disable_rclocal_command "$file"
            
            print_reboot_msg
            ;;

        list)
            echo "respawning commands:"
            
            for file in /usr/local/bin/respawn-*.sh; do
                if rclocal_command_enabled "$file"; then
                    cat "$file" | q='"' sed -ne "s/respawn_cmd=\"\(.\+\)\"/\1/p"
                fi
            done
            ;;
        
        *)
            print_usage respawn
            exit 1
    esac
}

function network() {
    iface="$1"
    subcmd="$2"
    
    case "$subcmd" in
        enable)
            echo "enabling network on interface $iface..."
            # TODO
            
            print_reboot_msg
            ;;
            
        disable)
            echo "disabling network on interface $iface..."
            # TODO
            
            print_reboot_msg
            ;;
        
        status)
            # TODO
            ;;
        
        *)
            print_usage network
            exit 1
    esac
}

function overclock() {
    subcmd="$1"
    
    case "$subcmd" in
        disable)
            echo "disabling overclocking..."
            disable_config "/boot/config.txt" "gpu_mem"
            disable_config "/boot/config.txt" "arm_freq"
            disable_config "/boot/config.txt" "gpu_freq"
            disable_config "/boot/config.txt" "core_freq"
            disable_config "/boot/config.txt" "sdram_freq"
            disable_config "/boot/config.txt" "over_voltage_sdram"
            disable_config "/boot/config.txt" "over_voltage"
            disable_config "/boot/config.txt" "force_turbo"
            disable_config "/boot/config.txt" "initial_turbo"
            disable_config "/boot/config.txt" "current_limit_override"
            disable_config "/boot/config.txt" "avoid_pwm_pll"
            
            print_reboot_msg
            ;;
            
        normal)
            echo "enabling normal overclocking..."
            set_config "/boot/config.txt" "gpu_mem" "16" '='
            set_config "/boot/config.txt" "arm_freq" "1000" '='
            disable_config "/boot/config.txt" "gpu_freq"
            set_config "/boot/config.txt" "core_freq" "500" '='
            set_config "/boot/config.txt" "sdram_freq" "600" '='
            disable_config "/boot/config.txt" "over_voltage_sdram"
            set_config "/boot/config.txt" "over_voltage" "6" '='
            disable_config "/boot/config.txt" "force_turbo" "0" '='
            disable_config "/boot/config.txt" "initial_turbo" "0" '='
            disable_config "/boot/config.txt" "current_limit_override"
            disable_config "/boot/config.txt" "avoid_pwm_pll" "0" '='
            
            print_reboot_msg
            ;;
        
        hardcore)
            echo "enabling hardcore overclocking..."
            set_config "/boot/config.txt" "gpu_mem" "16" '='
            set_config "/boot/config.txt" "arm_freq" "1050" '='
            set_config "/boot/config.txt" "gpu_freq" "500" '='
            set_config "/boot/config.txt" "core_freq" "500" '='
            set_config "/boot/config.txt" "sdram_freq" "600" '='
            set_config "/boot/config.txt" "over_voltage_sdram" "8" '='
            set_config "/boot/config.txt" "over_voltage" "8" '='
            set_config "/boot/config.txt" "force_turbo" "1" '='
            set_config "/boot/config.txt" "initial_turbo" "30" '='
            set_config "/boot/config.txt" "current_limit_override" "0x5A000020" '='
            set_config "/boot/config.txt" "avoid_pwm_pll" "1" '='
            
            print_reboot_msg
            ;;
        
        status)
            arm_freq=$(get_config /boot/config.txt arm_freq)
            over_voltage=$(get_config /boot/config.txt over_voltage)
            
            if [ -z "$arm_freq" ]; then
                echo "disabled"
                
            elif [ "$arm_freq" == "1000" ] && [ "$over_voltage" == "6" ]; then
                echo "normal"
                
            elif [ "$arm_freq" -ge "1050" ] && [ "$over_voltage" == "8" ]; then
                echo "hardcore"
                
            else
                echo "unknown"

            fi
            ;;
        
        *)
            print_usage overclock
            exit 1
    esac
}

function strip() {
    subcmd="$1"
    
    case "$subcmd" in
        gfx)
            echo "removing graphics-related packages..."
            apt-get -y remove --purge --auto-remove libx11-.* ttf-dejavu-core python-pifacecommon python3-pifacecommon plymouth\
                    omxplayer xdg-utils menu-xdg java-common desktop-file-utils libfreetype6-dev lxde-icon-theme
                    
            print_reboot_msg
            ;;
            
        full)
            apt-get -y remove --purge --auto-remove libx11-.* ttf-dejavu-core python-pifacecommon python3-pifacecommon plymouth\
                    omxplayer xdg-utils menu-xdg java-common desktop-file-utils libfreetype6-dev lxde-icon-theme\
                    iproute iptables iputils-ping isc-dhcp-common net-tools netbase netcat-openbsd netcat-traditional wireless-tools wpasupplicant\
                    libasound2 dphys-swapfile libglib2.0-0 libldap-2.4-2 libxapian-dev manpages manpages-dev pciutils penguinspuzzle usbutils xz-utils\
                    gcc-4.6 gdb strace

            print_reboot_msg
            ;;
        
        *)
            print_usage strip
            exit 1
    esac
}

function install() {
    subcmd="$1"
    
    case "$subcmd" in
        utils)
            echo "installing various utilities..."
            apt-get -y install htop rcconf tcpdump hdparm arping iotop
            ;;
            
        proftpd)
            echo "installing proftpd..."
            apt-get -y install proftpd
            
            # TODO
            print_reboot_msg
            ;;
            
        nginx)
            echo "installing nginx..."
            apt-get -y install nginx
            
            # TODO
            print_reboot_msg
            ;;
            
        samba)
            echo "installing samba..."
            # TODO
            print_reboot_msg
            ;;
            
        deluge)
            echo "installing deluge..."
            # TODO
            print_reboot_msg
            ;;
            
        *)
            print_usage install
            exit 1
    esac
}

function backup() {
    if [ -z "$1" ] || [ "$1" == "help" ]; then
        print_usage backup
        exit 1
    fi
    
    tarfile="$1"
    extra_exclude="${@:2}"
    
    exclude="
        --exclude=/proc
        --exclude=/sys
        --exclude=/tmp/*
        --exclude=/run/*
        --exclude=/mnt/*
        --exclude=/media/*
        --exclude=/lost+found
        --exclude=/var/swap"
    
    set -f
    for ee in $extra_exclude; do
        exclude="${exclude} --exclude=${ee}"
    done
    set +f
    
    exclude=(${exclude[@]})
    
    echo "creating $tarfile..."
    tar ${exclude[@]} -cvf "${tarfile}" /
    
    echo "Tar file ${tarfile} created. You can restore it issuing the following commands (on another system):"
    echo "    sudo tar -C /path/to/mounted/root/ -zxvf tarfile --exclude=boot/*"
    echo "    sudo tar -C /path/to/mounted/boot/ -zxvf tarfile --strip=1 boot"
}

# check for Raspbian
if ! check_raspbian > /dev/null 2>&1; then
    echo "this script can only be used on a Raspbian install!"
    exit 1
fi

# check for a command
if [ -z "$command" ]; then
    print_usage
    exit 0
fi

# check for root
if [ "$(id -u)" != "0" ]; then
    echo "this script must be run as root!"
    exit 1
fi


# main menu

case "$command" in
    coreinfo)
        coreinfo "${@:2}"
        ;;
    
    watchdog)
        watchdog "${@:2}"
        ;;
    
    softwatchdog)
        softwatchdog "${@:2}"
        ;;
    
    respawn)
        respawn "${@:2}"
        ;;
    
    network)
        network "${@:2}"
        ;;
    
    overclock)
        overclock "${@:2}"
        ;;
    
    strip)
        strip "${@:2}"
        ;;
    
    install)
        install "${@:2}"
        ;;
    
    backup)
        backup "${@:2}"
        ;;
    
    *)
        print_usage
        exit 1
esac

