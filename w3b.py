#!/usr/bin/env python2

import base64
import datetime
import gevent
import requests
import signal
import sys

from gevent import monkey, pool
from optparse import OptionParser

monkey.patch_socket()
monkey.patch_ssl()


request_no = 0
success_count = 0
fail_count = 0
options = None
last_progress = -1
before = None


def parse_options():
    global options
    
    parser = OptionParser(usage='usage: %prog [options] <url> [cookie1=value1;cookie2=value2] [data]')
    parser.add_option('-a', '--auth', help='use http basic authentication (e.g. user:pass)',
            type='string', dest='auth', default='')
    parser.add_option('-n', '--request-number', help='the total number of requests',
            type='int', dest='req_number', default=10)
    parser.add_option('-c', '--concurrency', help='the maximal number of concurrent requests',
            type='int', dest='concurrency', default=1)
    parser.add_option('-t', '--timeout', help='the request timeout, in seconds',
            type='int', dest='timeout', default=10)
    parser.add_option('-v', '--verbose', help='print some additional information',
            action='store_true', dest='verbose', default=False)
    
    (options, args) = parser.parse_args()
    
    try:
        options.url = args[0]
        
    except:
        parser.print_help()
        sys.exit()

    options.cookies = {}        
    if len(args) > 1 and args[1]:
        options.cookies = dict([(v.split('=', 1)) for v in args[1].split(';')])
    
    options.data = None
    if len(args) > 2:
        options.data = args[2]
    
    print('performing %d requests with a concurrency of %d' % (options.req_number, options.concurrency))


def sigint_handler(signal=None, frame=None):
    print_stats()
    sys.exit(0)


def debug(msg):
    if options.verbose:
        print(msg)
        

def milliseconds(delta):
    seconds = delta.days * 86400 + delta.seconds
    return int(seconds * 1000 + delta.microseconds / 1000)


def do_request():
    global fail_count, success_count, request_no, last_progress
    
    try:
        request_no += 1
        
        req_no = request_no
        debug('making request no %d/%d...' % (req_no, options.req_number))
        
        headers = {}
        cookies = {}
        if options.auth:
            token = base64.encodestring(options.auth)
            headers['Authorization'] = 'Basic ' + token

        headers['Connection'] = 'close'

        if options.data:
            request = requests.post(options.url, data=options.data, timeout=options.timeout,
                    headers=headers, cookies=options.cookies)

        else:
            request = requests.get(options.url, timeout=options.timeout,
                    headers=headers, cookies=options.cookies)

        if request.status_code / 100 != 2:
            raise Exception(str(request.status_code))

        success_count += 1
        
        debug('request no %d/%d succeeded' % (req_no, options.req_number))
    
    except Exception as e: #@UnusedVariable
        debug('request no %d/%d failed: %s' % (req_no, options.req_number, str(e)))
        fail_count += 1
    
    finally:
        progress = int(req_no * 100 / options.req_number)
        if progress > last_progress:
            if options.verbose:
                print('current progress: %d %%' % progress)
                
            else:
                sys.stdout.write('\r')
                sys.stdout.write('%d %%' % progress)
                sys.stdout.flush()
            
            last_progress = progress


def print_stats():
    if before is None:
        return
    
    after = datetime.datetime.now()
    delta = after - before

    print('\n------------')
    print('succeeded requests: %d/%d (%.2f %%)' % (success_count, options.req_number, success_count * 100.0 / options.req_number))
    print('failed requests:    %d/%d (%.2f %%)' % (fail_count, options.req_number, fail_count * 100.0 / options.req_number))
    print('total duration:     %.2f seconds' % (milliseconds(delta) / 1000.0))
    
    delta = milliseconds(delta)
    if delta == 0:
        rate = 0

    else: 
        rate = success_count * 1000.0 / delta
    
    print('request rate:       %.2f requests/second' % rate)


if __name__ == '__main__':
    parse_options()
    signal.signal(signal.SIGINT, sigint_handler)    
    gevent.signal(signal.SIGINT, sigint_handler)
    
    pool = pool.Pool(options.concurrency)
    
    before = datetime.datetime.now()
    for n in xrange(options.req_number):
        pool.spawn(do_request)

    pool.join()

    print_stats()
