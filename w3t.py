#!/usr/bin/env python2

import base64
import datetime
import functools
import signal
import sys

from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.ioloop import IOLoop
from optparse import OptionParser


request_no = 0
response_count = 0
success_count = 0
fail_count = 0
options = None
last_progress = -1
before = None


def parse_options():
    global options
    
    parser = OptionParser(usage='usage: %prog [options] <url> [cookie1=value1;cookie2=value2] [data]')
    parser.add_option('-a', '--auth', help='use http basic authentication (e.g. user:pass)',
            type='string', dest='auth', default='')
    parser.add_option('-n', '--request-number', help='the total number of requests',
            type='int', dest='req_number', default=10)
    parser.add_option('-c', '--concurrency', help='the maximal number of concurrent requests',
            type='int', dest='concurrency', default=1)
    parser.add_option('-t', '--timeout', help='the request timeout, in seconds',
            type='int', dest='timeout', default=10)
    parser.add_option('-v', '--verbose', help='print some additional information',
            action='store_true', dest='verbose', default=False)
    
    (options, args) = parser.parse_args()
    
    try:
        options.url = args[0]
        
    except:
        parser.print_help()
        sys.exit()

    options.cookies = {}        
    if len(args) > 1 and args[1]:
        options.cookies = dict([(v.split('=', 1)) for v in args[1].split(';')])
    
    options.data = None
    if len(args) > 2:
        options.data = args[2]
    
    print('performing %d requests with a concurrency of %d' % (options.req_number, options.concurrency))


def sigint_handler(signal=None, frame=None):
    print_stats()
    sys.exit(0)


def debug(msg):
    if options.verbose:
        print(msg)
        

def milliseconds(delta):
    seconds = delta.days * 86400 + delta.seconds
    return int(seconds * 1000 + delta.microseconds / 1000)


def do_requests():
    global fail_count, success_count, request_no, last_progress
    
    def on_response(req_no, response):
        global fail_count, success_count, request_no, last_progress, response_count
        
        if response.error:
            debug('request no %d/%d failed: %s' % (req_no, options.req_number, str(response.error)))
            fail_count += 1

        else:            
            debug('request no %d/%d succeeded' % (req_no, options.req_number))
            success_count += 1

        progress = int(req_no * 100 / options.req_number)
        if progress > last_progress:
            if options.verbose:
                print('current progress: %d %%' % progress)

            else:
                sys.stdout.write('\r')
                sys.stdout.write('%d %%' % progress)
                sys.stdout.flush()
            
            last_progress = progress
        
        response_count += 1
        if response_count >= options.req_number:
            IOLoop.instance().stop()

    for n in xrange(options.req_number):
        http_client = AsyncHTTPClient()

        headers = {'Connection': 'close'}
        if options.cookies:
            headers['Cookies'] = ';'.join([(k + '=' + v) for (k, v) in options.cookies.iteritems()])

        request = HTTPRequest(options.url, method=['GET', 'POST'][bool(options.data)], body=options.data or None,
                connect_timeout=options.timeout, request_timeout=options.timeout, headers=headers)

        if options.auth:
            request.auth_username, request.auth_password = options.auth.split(':', 1)

        request_no += 1
        
        debug('making request no %d/%d...' % (n, options.req_number))

        http_client.fetch(request, callback=functools.partial(on_response, n))
        

def print_stats():
    if before is None:
        return
    
    after = datetime.datetime.now()
    delta = after - before

    print('\n------------')
    print('succeeded requests: %d/%d (%.2f %%)' % (success_count, options.req_number, success_count * 100.0 / options.req_number))
    print('failed requests:    %d/%d (%.2f %%)' % (fail_count, options.req_number, fail_count * 100.0 / options.req_number))
    print('total duration:     %.2f seconds' % (milliseconds(delta) / 1000.0))
    
    delta = milliseconds(delta)
    if delta == 0:
        rate = 0

    else: 
        rate = success_count * 1000.0 / delta
    
    print('request rate:       %.2f requests/second' % rate)


if __name__ == '__main__':
    parse_options()
    signal.signal(signal.SIGINT, sigint_handler)
    
    import logging
    logging.basicConfig(level=logging.INFO)
    AsyncHTTPClient.configure('tornado.curl_httpclient.CurlAsyncHTTPClient', max_clients=options.concurrency)

    before = datetime.datetime.now()
    
    io_loop = IOLoop.instance()
    io_loop.add_callback(do_requests)
    io_loop.start()
    
    print_stats()

